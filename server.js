

var fs = require('fs');
const express = require("express");
const app = express();
var session = require('express-session');
const passport = require('passport');
var GitHubStrategy = require('passport-github').Strategy;
var cors = require('cors');
require('dotenv').config();

function validateUser(role,user){
  
  let dataPath;
  if(role==="superUser"){
     dataPath='./data/superUser.json';
  }
  else if(role==="user"){
    dataPath='./data/user.json';
  }
  //console.log('askjbd');
  
  
  try{
        var resp= fs.readFileSync(dataPath);
        let validUsers = JSON.parse(resp).users;
        console.log(validUsers);
        return validUsers.includes(user)?true:false;
     }
    catch(e){
      
      console.log('file read error',e);
      return false
  }  
}


function appendData(path,data){
  
    try{
          var resp = fs.readFileSync(path)
          let prev = JSON.parse(resp);
          prev.push(data);
          fs.writeFileSync(path,JSON.stringify(prev));
          return true;
        
      }
      catch(e){
        console.log('file parse/write error',e);
        return false
      }
    
}



passport.serializeUser(function(user, done) {
    done(null, user);
  });
  
passport.deserializeUser(function(user, done) {
    done(null, user);
  });



passport.use(new GitHubStrategy({
    clientID: "19349357f4e2786dba2b",
    clientSecret:"8929a434c149996935c116163303753a8a427e91",
    callbackURL: "https://open-rit-admin.herokuapp.com/auth/github/callback",
    scope:"user"
  },
  function(accessToken,refreshToken,profile,cb) {
    //console.log(profile);
    return cb(null, profile); 
  }
));


app.use(session({
  secret:"asjdnjkanfjbjkn",
  resave:true,
  saveUninitialized:true,
  cookie:{secure:false}
}));


app.use(express.json());
app.use(session({secret: "slakfnkajsnddl"}));
app.use(passport.initialize());
app.use(passport.session());
app.use(cors());


app.use(express.static("public"));
app.engine('html', require('ejs').renderFile);
app.use(express.urlencoded({
  extended: true
}));


app.get("/", (request, response) => {
  response.render(__dirname + "/views/index.html");
});


app.post("/role",(req,res)=>{
  
  res.json({"your role":req.body.role});
})



function ensureAuthenticated(req, res, next) {
    if (req.isAuthenticated())
      return next();
    else
      return res.redirect('/');
  }



app.post('/login',
  (req,res,next)=>{
  if(req.body.role==="superUser"){
    if(validateUser("superUser",req.body.email)){
      console.log(req.body.email);
      return next();
    }
    else{
      return res.json({"message":"email not registered"})
    }
  }},
 passport.authenticate('github',{failureRedirect:'/login/fail'}),
(req,res)=>{
  return res.json({"message":"authenticated !"})
}    
  )

app.get('/edit',
  ensureAuthenticated, 
  function(req, res) {
    res.render(__dirname+"/views/edit.html");
  }
);


app.get('/edit/event',
ensureAuthenticated,
(req,res)=>{
  console.log('here');
  res.render(__dirname+"/views/event.html")
});



app.post('/edit/event',
  ensureAuthenticated,
  function(req,res){
  let event =req.body;
  let start = event.date[0]+"T"+event.time[0];
  let end = event.date[1]+"T"+event.time[1];
  
  try{
    
  
  if(end>start){
    let apiEvent={...event}
    delete apiEvent.date;
    delete apiEvent.time;
    apiEvent['start']=start;
    apiEvent['end']=end;
    appendData('./data/events.json',apiEvent);
    event['created_by']=[req.user.emails[0].value,req.username,req.displayName];
    appendData('./data/logs.json',{type:"new event","log":event});
    res.render(__dirname+'/views/message.ejs',{'message':"event added succesfully"});
  }
    else{
      res.render(__dirname+'/views/message.ejs',{'message':"something went wrong! try again"});  
    }
  }
  catch(e){
    console.log(e);
    res.render(__dirname+'/views/message.ejs',{'message':"something went wrong! try again"});
  }
  
}
)

app.get('/auth/github/callback',
   passport.authenticate('github', { failureRedirect: '/login/fail' }),
  function(req, res) {
    console.log('callback recieved',req.user);
    if(validateUser('superUser',req.user.emails[0].value)){
      console.log('user validated')
       res.redirect('/edit');

       }
    else{
      console.log('user log out :email not registered')
      req.logout();
      res.redirect('/logout');
      }
}
);

app.get('/logout',(req,res)=>{
  req.logout();
  res.redirect('/');
});

app.get('/login/fail', 
  function(req, res) {
  console.log(req.body);
    res.status(401).send('Login failed');
  }
);




app.get('/api/events',(req,res)=>{
  var events = fs.readFileSync('./data/events.json');
  res.json(JSON.parse(events));
  
})


//general error handler
app.use(function(err, req, res, next) {
  console.log("Fatal error: " + JSON.stringify(err));
  next(err);
});


// listen for requests :)
const listener = app.listen(process.env.PORT, () => {
  console.log("Your app is listening on port " + listener.address().port);
});
